# Innovation-driven Entrepreneurs Club

The set of rules used to run the Tuesdays mornings structured fast-feedback sessions club for innovation-driven entrepreneurs.

The IEClub repo is an open-source private-data system, copyable by anyone wishing to activate their local community through feedback.

Play to win-win-win.

- Win for you.
- Win for us.
- Win for the environment, i.e.: everything not playing the in-the-moment game.

## Table of Contents

- [Who We Are](#who-we-are)
- [Interaction Protocol or API](#interaction-protocol-or-API)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License & Attribution](#license-attribution)

## Who We Are

Techonolgists, desginers and entrepreneurs teaming up for each-other's success.

## Why the Innovation-driven Entrepreneurs Club?

The most successful entrepreneurs execute well. The best also innovate.

Innovation thrives in diverse eclectic environments that integrate different disciplines to surface pragmatic actions.

**Event dynamics (minutes, hours)**
moderators, presenters, speaking, turn taking, broad feedback
Projects, Ongoing Activities, Short-term Value

1. Presenters gain feedback in a fast structured format, pertinent to the topic they raise. (presenter role)
2. Members evaluate and connect, and provide guidance based on the [SamErgo Interaction Protocol](https://gitlab.com/samergo/agents/game). (member role)
3. The most active members also selves-organize the regular meetings for everyone else. (maintainer role)

## Interaction Protocol or API

We rely on feedback as the core value-building block for innovation-driven entrepreneurs.

Today's segment-by-segment protocol consists of 3 stages:

- **the hashtag #** - what is it that just happened
- **the heart <3** - what is there to appreciate about what just happened
- **the delta Δ** - what to consider improving

This protocol is a living system, evolving, not enough for successful in-the-moment collaboration. To evolve it, send an MR.

## Install

The minimal requirement is another willing agent: 
- 2+ people or systems need to interact, 
- a scheduled time, and
- a place (can be online)

## Usage

1. Share an opportunity (need/want) with a set of observers to gain in-the-moment feedback.

2. Engage the attendees to evaluate your presentation in #-<3-Δ format.

For even better performance, co-create and broadcast the prepared topics (the agenda) in advance to all attending members.

## Maintainers

- Paul Cuciureanu [@pcuci](https://gitlab.com/pcuci)
- Leo Hartman [@leohartman](https://gitlab.com/leohartman)
- David Rowley [@david-ro](https://gitlab.com/david-ro)

The maintainers' job is to generate community consensus around the rules of the in-the-moment game-play, and integrate community feedback into this repostiroy. `TODO: How to become a maintainer?`

## Contributing

***You decide your level of involvement!*** *—Tyler Durden, Fight Club*

Feel free to dive in! [Open an issue](https://gitlab.com/samergo/agents/ieclub/issues/new) or submit MRs.

The IEClub follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.

## License & Attribution

The IEClub is available under the [Creative Commons CC0 1.0 License](https://creativecommons.org/publicdomain/zero/1.0/), meaning you are free to use it for any purpose, commercial or non-commercial, without any attribution back to the original authors (public domain).
