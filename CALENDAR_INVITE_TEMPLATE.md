Attending on-line?

- open Chromium
- hit Hangouts link inside Google calendar appointment
- mute your mic until you speak

Agenda:
- pick roles (facilitator, speakers, evaluators, timer, etc.)

(repeat multiple times)

- prepped content [3-7mins each] OR improv topic [2-4mins each]
- feedback: react to prepared content in silence-#-<3-delta format
- one minute silence - introspect and reflect, mentally prepare the next segments

(round robin, everyone shares feedback, fast-structured, for each presented topic)

- hashtag # - tweet what was presented
- heart <3 - what did you like, the positives
- deltas - consider improving, what to change


(at the end of the meeting)
- general meeting feedback and group evaluation